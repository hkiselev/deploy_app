require "rvm/capistrano"
require "bundler/capistrano"
# 1
set :application, "Deploy app."
set :repository,  "https://hkiselev@bitbucket.org/hkiselev/deploy_app.git"
# 2
set :deploy_to, "/home/deploy/projects/deploy_app"
# 3
set :scm, :git
set :branch, "master"
# 4
set :user, "deploy"
# password for deploy user, if RSA not setup
# set :scm_passphrase, "password"
# 5
set :use_sudo, false
#6
set :rails_env, "production"
#7
set :deploy_via, :copy # remote_cache method require user and password to git-repo service
#8
set :ssh_options, { :forward_agent => true }
#9
set :keep_releases, 5
#10
default_run_options[:pty] = true
#11
server "192.168.1.104", :app, :web, :db, :primary => true

namespace :deploy do
    desc "Symlink shared config files"
		task :symlink_config_files do
		    run "#{ try_sudo } ln -s #{ deploy_to }/shared/config/database.yml #{ current_path }/config/database.yml"
		end
		desc "Restart Passenger app"
		task :restart do
		    run "#{ try_sudo } touch #{ File.join(current_path, 'tmp', 'restart.txt') }"
		end
		desc "New assets"
		task :assets do
    	run "cd #{current_path} && bundle exec rake assets:precompile RAILS_ENV=#{rails_env}"
  	end
end

after "deploy", "deploy:symlink_config_files"
after "deploy", "deploy:restart"
after "deploy", "deploy:assets"
after "deploy", "deploy:migrate"
after "deploy", "deploy:cleanup"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

#role :web, "your web-server here"                          # Your HTTP server, Apache/etc
#role :app, "your app-server here"                          # This may be the same as your `Web` server
#role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end