# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DeployApp::Application.config.secret_key_base = 'b5bcd3561700f4aee2e105c9bfe6537b5c0adc5ae36b537afb9b4a59b6084af3713d6de810698636bfe1fc45b8dd1d4a05ebc41975689da2e7227eeafa451352'
